#!/bin/sh -e
#
# Vale.sh wrapper script
#
# Copyright (C) 2024 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

REQUIREMENTS="
find
vale
xargs
"

export LC_ALL=C

requirements() {
	for req in $REQUIREMENTS; do
		if ! which "$req" >/dev/null; then
			if [ -z "$has_missing" ]; then
				echo "$(basename "$0"): Missing script requirements!" 1>&2
				echo "Please install:" 1>&2
				has_missing=1
			fi
			echo " - '$req'" 1>&2
		fi
	done
	if [ -n "$has_missing" ]; then
		exit 1
	fi
}

usage() {
	cat 1>&2 <<EOF
Usage: $(basename "$0") [OPTIONS]... PATTERN [PATTERN]...
Lint prose with Vale.sh

Options:
  -i <pattern>              File name shell pattern to ignore;
                             Can be specified multiple times
  --help                    Print usage

  PATTERN                   File name base shell pattern to match
EOF
	exit 1
}

parse_args() {
	while [ $# -gt 0 ]; do
		arg="$1"
		case $arg in
			-i)
				shift
				if [ -z "$1" ]; then
					usage
				fi
				ignore_patterns="$ignore_patterns -o -path '$1'"
				;;
			*)
				patterns="$patterns -o -name '$1'"
				;;
		esac
		shift
	done
}

main() {
	requirements
	parse_args "$@"
	if [ -z "$patterns" ]; then
		usage
	fi
	eval "find . ${ignore_patterns:+-type d \( ${ignore_patterns# -o} \) -prune -o }-type f \( ${patterns# -o} \) -print0" | xargs -0 vale
}

main "$@"
