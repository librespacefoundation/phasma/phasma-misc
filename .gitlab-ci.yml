---
variables:
  GITLAB_CI_IMAGE_ALPINE: 'alpine:3.16.2'
  GITLAB_CI_IMAGE_DEBIAN: 'debian:bookworm'
  # CONNVIZ_REPO: 'https://gitlab.com/drid/connwiz.git'
  GITLAB_CI_IMAGE_PYTHON: 'python:3.12.1-alpine3.19'
  GITLAB_CI_IMAGE_KICAD: 'kicad/kicad:8.0.4'
  SCH_FILE_PATH: 'adcs-adapter/adcs-adapter.kicad_sch'
  PCB_FILE_PATH: 'adcs-adapter/adcs-adapter.kicad_pcb'
  GITLAB_CI_DOCS_PATH: 'GSE-Docs/'
stages:
  - static
  - kicad
  - build
  - deploy

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - project: 'librespacefoundation/templates/gitlab-ci'
    ref: 'ed44d9741ba9d13ff097b85cbd6ce7c11af3ee8c'
    file: '.gitlab-ci-docs.yml'

# 'static' stage
sign_off:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_ALPINE}
  before_script:
    - apk add --no-cache git
  script: >-
    git log
    --grep "^Signed-off-by: .\+<.\+\(@\| at \).\+\(\.\| dot \).\+>$"
    --invert-grep
    --format="Detected commit '%h' with missing or bad sign-off! Please read 'CONTRIBUTING.md'."
    --exit-code

lint:
  stage: static
  needs: []

wireviz:
  stage: build
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - apt update
    - apt install -y python3-pip git graphviz
    - pip install wireviz==0.4.1 --break-system-packages
  script:
    - wireviz -o cabling/phasma-harness phasma-battery-cables.yaml
    - wireviz -o cabling/phasma-harness phasma-fpga-rgmii-debug-cables.yaml
    - wireviz -o cabling/phasma-harness phasma-hdrm-cables.yaml
    - wireviz -o cabling/phasma-harness phasma-jtag-cables.yaml
    - wireviz -o cabling/phasma-harness phasma-rf-cables.yaml
    - wireviz -o cabling/phasma-harness phasma-solar-panel-cables.yaml
    - wireviz -o cabling/umbilical umbilical.yaml
    - wireviz -o cabling/TVAC_Harness tvac-harness.yaml
  artifacts:
    paths:
      - cabling
    when: on_success
    expire_in: 30 days

# connviz:
#   stage: build
#   needs: ["wireviz"]
#   image: ${GITLAB_CI_IMAGE_DEBIAN}
#   before_script:
#     - apt update
#     - apt install -y python3-pip git graphviz
#     - git clone ${CONNVIZ_REPO} connviz
#     - cd connviz
#     - pip install -r requirements.txt --break-system-packages
#   script:
#     - python3 connviz.py -o ../../cabling/phasma-harness-block ../phasma-harness.yaml
#   artifacts:
#     paths:
#       - cabling
#     when: on_success
#     expire_in: 30 days

ERC:
  stage: kicad
  needs: ["sign_off"]
  image: ${GITLAB_CI_IMAGE_KICAD}
  script:
    - kicad-cli sch erc --define-var LSF_KICAD_LIB=./lsf-kicad-lib ${SCH_FILE_PATH} --exit-code-violations
  after_script:
    - cat adcs-adapter.rpt
  rules:
    - changes:
      - adcs-adapter
      - gitlab-ci.yml
  artifacts:
    paths:
      - "adcs-adapter.rpt"
    untracked: false
    access: all
    expire_in: 30 days

DRC:
  stage: kicad
  needs: ["sign_off"]
  image: ${GITLAB_CI_IMAGE_KICAD}
  before_script:
    - git clone https://gitlab.com/librespacefoundation/lsf-kicad-lib.git lsf-kicad-lib
  script:
    - kicad-cli pcb drc --define-var LSF_KICAD_LIB=./lsf-kicad-lib --severity-error --severity-exclusions --exit-code-violations ${PCB_FILE_PATH}
  after_script:
    - cat adcs-adapter.rpt
  rules:
    - changes:
      - adcs-adapter
      - gitlab-ci.yml
  artifacts:
    paths:
      - "adcs-adapter.rpt"
    untracked: false
    access: all
    expire_in: 30 days

Production:
  stage: kicad
  needs: ["sign_off"]
  image: ${GITLAB_CI_IMAGE_KICAD}
  before_script:
    - sudo apt-get update
    - sudo apt-get install -y python3-pip python3-venv
    - python3 -m venv --system-site-packages ./venv
    - source ./venv/bin/activate
    - pip install InteractiveHtmlBom
    - git clone https://gitlab.com/librespacefoundation/lsf-kicad-lib.git lsf-kicad-lib
    - export INTERACTIVE_HTML_BOM_NO_DISPLAY=1
  script:
    - kicad-cli sch export bom ${SCH_FILE_PATH} --exclude-dnp --fields 'Reference,Value,Footprint,${QUANTITY},${DNP},Part Number' --group-by 'Value, Part Number'
    - kicad-cli pcb export step ${PCB_FILE_PATH}
    - mkdir gerbers
    - kicad-cli pcb export drill ${PCB_FILE_PATH} -o gerbers/
    - kicad-cli pcb export gerbers ${PCB_FILE_PATH} -o gerbers --no-x2
    - generate_interactive_bom --no-browser --dest-dir ../ --name-format "%f_$r-%p" ${PCB_FILE_PATH}
  only:
    - tags
  artifacts:
    paths:
      - "*.step"
      - "*.csv"
      - "gerbers/"
      - "*.html"
    untracked: false
    when: on_success
    access: all

# 'build' stage
docs:
  stage: build
  needs: []
  artifacts:
    paths:
      - ${GITLAB_CI_DOCS_PATH}_build/html
      # - ${GITLAB_CI_DOCS_PATH}_build/latex

# 'deploy' stage
pages:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
