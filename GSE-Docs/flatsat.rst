PHASMA FlatSat Manual
=====================

EPS Operating Modes and setup
-----------------------------
EPS Can be used with a battery connected or with a simulated battery

Connect EPS on the long side of FlatSat to improve stability due to battery and EPS weight

Add jumpers on
    * VBat_L0
    * SCL
    * SDA
    * 3V3_L1
    * 5V_L0

Battery Operation
~~~~~~~~~~~~~~~~~

#. Make sure all RBF/KillSwitch jumpers are connected to the battery
#. Connect the battery on top of the EPS

Simulated Battery Operation with SA Charging
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Battery is simulated using a PSU and DC Load connected in parallel

.. table:: PSU and DC Load settings
   :widths: auto

   ===============  ============
   Setting          Value
   ===============  ============
   PSU Voltage      6.16V - 8.3V
   PSU Current      16A
   DC Load Mode     CC
   DC Load Current  8A
   ===============  ============

**Equipment:**

* DC Load
* PSU capable of 8.3V 3A
* Cables for PSU and DC Load

.. note::
    Set voltage limit to 8.3V on the PSU in order to avoid damage to EPS

**Connections:**
Connect PSU and DC Load to FlatSat VIn Banana Sockets. PSU and DC Load are connected in parallel.

Simulated Battery Operation without SA Charging
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Equipment:**

* PSU capable of 8.3V 3A
* Cables for PSU

.. note::
    Set voltage limit to 8.3V on the PSU in order to avoid damage to EPS

**Connections:**
Connect PSU to FlatSat VIn Banana Sockets.


Simulated Solar Array
~~~~~~~~~~~~~~~~~~~~~

**Equipment:**

* PSU 5V 2W
* Resitor 7.5Ω 2W
* Cable for connecting to S1A1 EPS connector (DF13-5S-1.25C)

**Procedure**

#. Connect the resistor in series with the positive line
#. Set V=18.62V 867mA
#. Turn on PSU to simulate SA

Battery Charging
----------------
When testing with a battery connected on the EPS it is recommended keep it under charging to avoid deep discharge.
The same procedure can be used for charging the battery

**Equipment:**

* PSU 5V 2W

**Charging via VIn**

#. Connect the battery on top of EPS
#. Connect a jumper from VIn to Charge pin on FlatSat
#. Connect a PSU set to 5V 1A to VIn Banana Sockets
#. Remove all RBF jumpers
#. Turn on PSU

**Charging via H1 header**

#. Connect the battery on top of EPS
#. Connect a cable from PSU to Charge pin on FlatSat (EPS Side) or H1-32
#. Remove all RBF jumpers
#. Turn on PSU

Maximum power consumption should be under 2W

Using FlatSat during development
--------------------------------

.. warning::
    Never allow the battery to discharge unless during discharge testing

    A "USB" charger should always be connected

**Before using, connecting or disconnecting systems**

* Make sure all RBFs are inserted to battery (when used) and all power supplies are turned off.
* Connect or disconnect systems
* Remove RBFs and turn on power supplies

**After using**

Turn off power supplies and insert all RBFs
