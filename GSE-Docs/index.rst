.. PHASMA FlatSat Manual documentation master file, created by
   sphinx-quickstart on Wed Sep 25 14:42:58 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

PHASMA GSE Manual
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   flatsat

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
