# Miscellaneous Repository
Welcome to the PHASMA Miscellaneous repository! This repository serves as a home for various projects, scripts, and other resources that don't quite fit into specific categories or repositories

# Contents

## Harness
These diagrams are rendered using [WireViz](https://github.com/wireviz/WireViz) providing clear visualizations of the harness layout and connectivity

[View latest generated harnesses](https://gitlab.com/librespacefoundation/phasma/phasma-misc/-/jobs/artifacts/main/browse/cabling?job=wireviz)

The block level harness:
* [PHASMA block level harness - Sheet 1/2](https://gitlab.com/librespacefoundation/phasma/phasma-misc/-/blob/main/phasma-cable-harness-1.svg?ref_type=heads)
* [PHASMA block level harness - Sheet 2/2](https://gitlab.com/librespacefoundation/phasma/phasma-misc/-/blob/main/phasma-cable-harness-2.svg?ref_type=heads)

## Documentation

All documents are rendered at https://librespacefoundation.gitlab.io/phasma/phasma-misc

* FlatSat Manual
